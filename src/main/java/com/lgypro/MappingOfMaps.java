package com.lgypro;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MappingOfMaps {
    public static void main(String[] args) {
        Map<String, List<String>> employees = new HashMap<>();
        employees.put("A", Arrays.asList("Andreas", "Arnold", "Aden"));
        employees.put("C", Arrays.asList("Christian", "Carter"));
        employees.put("M", Arrays.asList("Marcus", "Mary"));

        Gson gson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
        System.out.println(gson.toJson(employees));

        String dollarJson = """
                {
                  "1$": {
                    "amount": 1,
                    "currency": "Dollar"
                  },
                  "2$": {
                    "amount": 2,
                    "currency": "Dollar"
                  },
                  "3€": {
                    "amount": 3,
                    "currency": "Euro"
                  }
                }
                                """;
        TypeToken<Map<String, AmountWithCurrency>> amountCurrencyType = new TypeToken<>() {
        };
        Map<String, AmountWithCurrency> amountCurrency = gson.fromJson(dollarJson, amountCurrencyType);
        System.out.println("type = " + amountCurrency.getClass().getName());
        amountCurrency.forEach((key, value) -> System.out.println(key + ": " + value));
    }
}
