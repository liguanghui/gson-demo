package com.lgypro;

public class AmountWithCurrency {
    String currency;
    int amount;

    @Override
    public String toString() {
        return "AmountWithCurrency{" +
                "currency='" + currency + '\'' +
                ", amount=" + amount +
                '}';
    }
}
