package com.lgypro;

public class Waiter {
    String name;
    int age;
    int salary;

    @Override
    public String toString() {
        return "Waiter{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", salary=" + salary +
                '}';
    }
}
