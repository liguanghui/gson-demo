package com.lgypro;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.HashSet;
import java.util.Set;

public class MappingOfSets {
    public static void main(String[] args) {
        Set<String> users = new HashSet<>();
        users.add("Christian");
        users.add("Marcus");
        users.add("Norman");
        users.add("Marcus"); // would not be added again
        System.out.println(users);

        Gson gson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
        System.out.println(gson.toJson(users));

        String founderJson = """
                [
                    {
                      "name": "Christian",
                      "flowerCount": 1
                    },
                    {
                      "name": "Marcus",
                      "flowerCount": 3
                    },
                    {
                      "name": "Norman",
                      "flowerCount": 2
                    }
                ]
                                """;
        TypeToken<Set<Founder>> founderSetType = new TypeToken<>() {
        };
        Set<Founder> founders = gson.fromJson(founderJson, founderSetType);
        System.out.println("type = " + founders.getClass().getName());
        System.out.println(founders);
    }
}
