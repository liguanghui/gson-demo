package com.lgypro;

import com.google.gson.Gson;


public class SerializationOfEnums {
    public static void main(String[] args) {
        UserDayEnum userObject = new UserDayEnum("Norman", "norman@fs.io", true, 26, DayOfWeek.SUNDAY);

        Gson gson = new Gson();
        String userWithEnumJson = gson.toJson(userObject);
        System.out.println(userWithEnumJson);

        System.out.println(gson.fromJson(userWithEnumJson, UserDayEnum.class));
    }
}
