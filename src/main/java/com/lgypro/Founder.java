package com.lgypro;

public class Founder {
    String name;
    int flowerCount;

    @Override
    public String toString() {
        return "Founder{" +
                "name='" + name + '\'' +
                ", flowerCount=" + flowerCount +
                '}';
    }
}
