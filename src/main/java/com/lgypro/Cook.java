package com.lgypro;

public class Cook {
    String name;
    int age;
    int salary;

    @Override
    public String toString() {
        return "Cook{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", salary=" + salary +
                '}';
    }
}
