package com.lgypro;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class SerializationOfSpecialFloatingPointValues {
    public static void main(String[] args) {
        Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues()
                .setPrettyPrinting()
                .create();

        UserFloat user = new UserFloat("Norman", Float.POSITIVE_INFINITY);

        String usersJson = gson.toJson(user); // will throw an exception
        System.out.println(usersJson);
    }
}
