package com.lgypro;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ParseJsonTree {
    public static void main(String[] args) {
        String json = """
                {
                   "title": "Free Music Archive - Albums",
                   "message": "",
                   "errors": null,
                   "total": "11259",
                   "totalPages": 2252,
                   "page": 1,
                   "limit": "5",
                   "dataset": [
                     {
                       "album_id": "7596",
                       "album_title": "Album 1",
                       "album_images": [
                         {
                           "album_id": null,
                           "image_id": "1",
                           "user_id": null
                         }
                       ]
                     }
                   ]
                 }
                                 """;
        JsonObject root = JsonParser.parseString(json).getAsJsonObject();
        System.out.println(root);
        root.asMap().forEach((name, element) -> {
            System.out.println(name + ": " + element);
        });
        JsonArray dataset = root.get("dataset").getAsJsonArray();
        System.out.println(dataset);
        System.out.println("size = " + dataset.size());
        JsonObject images = dataset.get(0).getAsJsonObject();
        System.out.println(images);
        images.entrySet().forEach(entry -> {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        });
        String albumTitle = images.get("album_title").getAsString();
        System.out.println(albumTitle);
        JsonArray albumImages = images.get("album_images").getAsJsonArray();
        System.out.println(albumImages);
        albumImages.asList().forEach(element -> {
            JsonObject object = element.getAsJsonObject();
            object.asMap().forEach((name, value) -> {
                System.out.println(name + ": " + value);
            });
        });
    }
}
