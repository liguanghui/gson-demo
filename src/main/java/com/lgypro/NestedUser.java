package com.lgypro;

public class NestedUser {
    String name;
    String email;
    int age;
    boolean isDeveloper;

    UserAddress userAddress;

    public NestedUser(String name, String email, int age, boolean isDeveloper, UserAddress userAddress) {
        this.name = name;
        this.email = email;
        this.age = age;
        this.isDeveloper = isDeveloper;
        this.userAddress = userAddress;
    }

    @Override
    public String toString() {
        return "NestedUser{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", age=" + age +
                ", isDeveloper=" + isDeveloper +
                ", userAddress=" + userAddress +
                '}';
    }
}
