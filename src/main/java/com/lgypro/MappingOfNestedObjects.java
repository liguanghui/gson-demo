package com.lgypro;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class MappingOfNestedObjects {
    public static void main(String[] args) {
        SimpleUser userObject = new SimpleUser(
                "Norman",
                "norman@futurestud.io",
                26,
                true
        );
        System.out.println(userObject);
        Gson gson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
        System.out.println(gson.toJson(userObject));


        String userJson = "{'age':26,'email':'norman@futurestud.io','isDeveloper':true,'name':'Norman'}";
        SimpleUser simpleUser = gson.fromJson(userJson, SimpleUser.class);
        System.out.println(simpleUser);

        System.out.println("@@@@@@@@@@@@@@@@@@@@");

        UserAddress userAddress = new UserAddress(
                "Main Street",
                "42A",
                "Magdeburg",
                "Germany"
        );

        NestedUser nestedUser = new NestedUser(
                "Norman",
                "norman@futurestud.io",
                26,
                true,
                userAddress
        );
        System.out.println(gson.toJson(nestedUser));

        System.out.println("@@@@@@@@@@@@@@@@@@@@");

        String restaurantJson = """
                                {
                  "name": "Future Studio Steak House",
                  "owner": {
                    "name": "Christian",
                    "address": {
                      "city": "Magdeburg",
                      "country": "Germany",
                      "houseNumber": "42A",
                      "street": "Main Street"
                    }
                  },
                  "cook": {
                    "age": 18,
                    "name": "Marcus",
                    "salary": 1500
                  },
                  "waiter": {
                    "age": 18,
                    "name": "Norman",
                    "salary": "1000"
                  }
                }
                                """;
        Restaurant restaurant = gson.fromJson(restaurantJson, Restaurant.class);
        System.out.println(restaurant);
    }
}
