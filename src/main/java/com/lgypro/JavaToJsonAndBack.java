package com.lgypro;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class JavaToJsonAndBack {
    public static void main(String[] args) {
        GsonBuilder builder = new GsonBuilder();
        builder.serializeNulls();
        builder.setPrettyPrinting();
        builder.setFieldNamingStrategy(f -> {
            if (f.getName().equals("albumId")) {
                return "album_id";
            } else {
                return f.getName();
            }
        });
        Gson gson = builder.create();

        AlbumImages image = new AlbumImages();
        image.image_id = "1";
        System.out.println(gson.toJson(image, AlbumImages.class));
        System.out.println("--------------------");

        Dataset dataset = new Dataset();
        dataset.album_id = "7596";
        dataset.album_title = "Album 1";
        dataset.images.add(image);
        System.out.println(gson.toJson(dataset));
        System.out.println("--------------------");

        Albums albums = new Albums();
        albums.title = "Free Music Archive - Albums";
        albums.message = "";
        albums.total = "11259";
        albums.totalPages = 2252;
        albums.page = 1;
        albums.limit = "5";
        albums.dataset.add(dataset);

        System.out.println(gson.toJson(albums));
    }
}
