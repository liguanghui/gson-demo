package com.lgypro;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MappingOfListsOfObjects {
    public static void main(String[] args) {
        List<RestaurantMenuItem> menu = new ArrayList<>();
        menu.add(new RestaurantMenuItem("Spaghetti", 7.99f));
        menu.add(new RestaurantMenuItem("Steak", 12.99f));
        menu.add(new RestaurantMenuItem("Salad", 5.99f));

        RestaurantWithMenu restaurant =
                new RestaurantWithMenu("Future Studio Steak House", menu);

        Gson gson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
        String restaurantJson = gson.toJson(restaurant);
        System.out.println(restaurantJson);

        System.out.println("@@@@@@@@@@@@@@@@@@@@");

        System.out.println(gson.toJson(menu));

        String founderJson = """
                [
                    {
                      "name": "Christian",
                      "flowerCount": 1
                    },
                    {
                      "name": "Marcus",
                      "flowerCount": 3
                    },
                    {
                      "name": "Norman",
                      "flowerCount": 2
                    }
                ]
                                """;

        Founder[] founders = gson.fromJson(founderJson, Founder[].class);
        Arrays.stream(founders).forEach(System.out::println);

        TypeToken<List<Founder>> founderListType = new TypeToken<>() {
        };
        List<Founder> founders1 = gson.fromJson(founderJson, founderListType);

        System.out.println("@@@@@@@@@@@@@@@@@@@@");
        System.out.println("type = " + founders1.getClass().getName());
        founders1.forEach(System.out::println);

        String generalInfoJson = """
                {
                  "name": "Future Studio Dev Team",
                  "website": "https://futurestud.io",
                  "founders": [
                    {
                      "name": "Christian",
                      "flowerCount": 1
                    },
                    {
                      "name": "Marcus",
                      "flowerCount": 3
                    },
                    {
                      "name": "Norman",
                      "flowerCount": 2
                    }
                  ]
                }
                                """;

        GeneralInfo generalInfo = gson.fromJson(generalInfoJson, GeneralInfo.class);
        System.out.println(generalInfo);
    }
}
