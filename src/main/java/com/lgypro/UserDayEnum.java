package com.lgypro;

public class UserDayEnum {
    private String name;
    private String email;
    private boolean isDeveloper;
    private int age;

    private DayOfWeek day;

    public UserDayEnum(String name, String email, boolean isDeveloper, int age, DayOfWeek day) {
        this.name = name;
        this.email = email;
        this.isDeveloper = isDeveloper;
        this.age = age;
        this.day = day;
    }

    @Override
    public String toString() {
        return "UserDayEnum{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", isDeveloper=" + isDeveloper +
                ", age=" + age +
                ", day=" + day +
                '}';
    }
}
