package com.lgypro;

import java.util.Arrays;
import java.util.List;

public class GeneralInfo {
    String name;
    String website;
    // List<Founder> founders;
    Founder[] founders;

    @Override
    public String toString() {
        return "GeneralInfo{" +
                "name='" + name + '\'' +
                ", website='" + website + '\'' +
                ", founders=" + Arrays.toString(founders) +
                '}';
    }
}
