package com.lgypro;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class MappingOfNullValues {
    public static void main(String[] args) {
        SimpleUser userObject = new SimpleUser(null, "norman@futurestud.io", 26, true);
        System.out.println(userObject);
        Gson gson = new GsonBuilder()
                .serializeNulls()
                .setPrettyPrinting().create();
        System.out.println(gson.toJson(userObject));

        String userJson = """
                {
                  "age": 26,
                  "isDeveloper": true,
                  "name": "Norman"
                }
                                """;
        SimpleUser user = gson.fromJson(userJson, SimpleUser.class);
        System.out.println(user);
    }
}
